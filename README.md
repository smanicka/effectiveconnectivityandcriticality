# A manual for interpreting the simulation code and data

This repository contains all the R code and a folder called ‘Data’ containing all the data (suffixed with ‘.Rdata’) files that were used for the purpose of the simulation of the ensembles referred to in the main paper. 

## Summary
The simulation code samples the look-up tables (LUT) from the data subfolder to generate the ensembles, then simulates them and finally generates the results files.

## Data
•	The files in the data folder contain the full set of the LUTs that the ST and CT simulations sampled from. 

•	The organization of the file contents is reflected in its naming convention: “SimDataK[k]P[p_min]to[p_max][K_type].Rdata”, where ‘k’ indicates the number of the inputs to the LUT; ‘p_min’ and ‘p_max’ refer to the minimum and maximum values of p of the LUTs that the file contains; and ‘K_type’ is equal to either ‘K’ referring to the files that the ST ensembles sampled from or ‘K_e’ referring to the files that the CT ensembles sampled from. 

•	Each file contains an R list, the number of whose elements equals the number of distinct ensembles (ST or CT) that were generated (each ensemble consists of a number of sample networks whose ensemble characteristics are the same). The list elements were organized to satisfy the specific parameter intervals that were chosen for this study (see main text and SI for details).

•	Each element in the list further consists of sub-elements namely $Funcs, $P, $K_e and $K, where $Funcs is a matrix whose columns list the outputs of the LUT associated with the input values assumed to be sorted in lexicographic order (the actual input values were thus not included in the files due to this assumption); $P consists of the list of the p values of the LUTs listed in $Funcs; $K_e lists the associated k_e values and $K lists the associated k values (this information is redundant since the k is already listed in the file name).

## Code
•	The main simulation files are named with the following convention: “SimNKEnsemblesK[k][K_type].R”, where ‘k’ indicates the in-degree of the nodes in the ensembles; and ‘K_type’ is either equal to ‘K’ referring to the ST simulations or ‘K_e’ referring to the CT simulations.

•	All of the main simulation files call the core simulation file “SimNKEnsembles.R” that actually generates the networks and simulates them.

•	The file “SimNKEnsembles.R” further calls “ComputePerturbationStats.R” that computes the essential statistics (mean, standard deviation, boxplot stats and confidence intervals) of the number of state-flips following a certain initial number of flips recorded at various time steps. These stats were used to compute the Derrida parameter (see main text for details).
